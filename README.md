# Awesome Bot Telegram Indonesia

![visit](https://badges.pufler.dev/visits/telegrambotindonesia/awesome-bot-telegram-indonesia) ![GitHub last commit](https://img.shields.io/github/last-commit/telegrambotindonesia/awesome-bot-telegram-indonesia) ![GitHub issues](https://img.shields.io/github/issues/telegrambotindonesia/awesome-bot-telegram-indonesia) ![GitHub pull requests](https://img.shields.io/github/issues-pr/telegrambotindonesia/awesome-bot-telegram-indonesia) ![GitHub closed pull requests](https://img.shields.io/github/issues-pr-closed/telegrambotindonesia/awesome-bot-telegram-indonesia) ![GitHub contributors](https://img.shields.io/github/contributors/telegrambotindonesia/awesome-bot-telegram-indonesia)

Berkolaborasi dipersilakan dengan mengclone dan membuat pull request.

Repo yang (berusaha) akan _uptodate_ untuk memuat tentang segala hal bot indonesia. Belajar, komunitas, daftar bot, dan segala hal tentang bot dalam negeri.

![GitHub forks](https://img.shields.io/github/forks/telegrambotindonesia/awesome-bot-telegram-indonesia?style=social) ![GitHub Repo stars](https://img.shields.io/github/stars/telegrambotindonesia/awesome-bot-telegram-indonesia?style=social)


<!-- mulai dari sini ya perubahannya -->

## Grup dan Channel

Ruang diskusi grup dan channel pada Telegram dalam bahasa indonesia yang akan membantu belajar dan mengembangkan kemampuanmu dalam membuat bot telegram.

- [@botindonesia](https://t.me/botindonesia) diskusi umum segala rupa bahasa pemrograman membahas bot telegram
- [@botphp](https://t.me/botphp) khusus membahas bot telegram dalam bahasa PHP
- [@ubotindonesia](https://t.me/ubotindonesia) diskusi khusus membahas user bot
- [@botkoleksi](https://t.me/botkoleksi) channel informasi bot yang patut dikoleksi

## Tutorial

### Google Apps Script

- [Materi Membuat Bot Telegram GRATIS dengan Google Apps Script](https://s.id/gasbot)


### Tutorial Made
- [Cara deploy bot music Telegram di termux](https://madewgn.github.io/artikel/cara-deploy-bot-music-di-termux/)
- [belajar membuat bot telegram sederhana dengan python](https://madewgn.github.io/artikel/cara-membuat-bot-telegram-sederhana-dengan-python/)

## Library & Framework

- [PHPTeleBot](https://github.com/radyakaze/phptelebot) library Telegram untuk PHP
- [Lumpia](https://lumpia.js.org) Framework GAS edisi ke 3
- [duaGram](https://github.com/ubotindonesia/duagram) userbot berbasis [gramjs]
- [TGSnake](https://github.com/butthx/tgsnake) userbot berbasis [gramjs]
- [HSubot](https://github.com/banghasan/hsubot) userbot berbasis [TDLib](https://github.com/tdlib/td)
- [String Generator](https://telegram.banghasan.com/ubotstring/) untuk userbot berbasis [gramjs]


## Hosting

Hanya yang gratis saja di sini ya..

- [Google Apps Script](https://script.google.com) -- berbasis javascript, hanya untuk bot api
- [Heroku](https://www.heroku.com/) bisa untuk bot api dan userbot, bahasa pemrograman bebas
- [AWS Free](https://aws.amazon.com/id/free/) vps free tier 1 tahun, perlu kartu kredit untuk mengaktifkan.
- [Azure Free](https://azure.microsoft.com/en-us/free/) ada free credit sebesar 200$, perlu credit card untuk mengaktifkan
- [Replit Free](https://replit.com/site/pricing) bisa untuk deploy ± semua bahasa pemrograman.
---

## Daftar Bot

Silakan ditambahkan bot kalian di sini yang masih aktif. Bot hanya buatan anak negeri - Indonesia.

Bot yang sudah tidak aktif agar dihapus atau dicoret.

### Group / Channel

Pengelolaan Group ataupun Channel

- ~~@gedebugbot~~ non aktif

### Fitur Khusus

Bot karya anak negeri yang memiliki fitur khusus.

- [@strukturBot](https://t.me/strukturbot) cek id user, group ataupun channel. Juga berfungsi untuk deteksi info struktur pesan dalam format JSON guna membantu para developer bot
- [@apidocsbot](https://t.me/apidocsbot) bot yang membantu menginformasikan tentang method bot API
- [@oneGooglebot](https://t.me/oneGooglebot) bot berbasis GAS, memiliki fungsi penerjemah, peta, dan OCR.
- [@YT_Thumbnail_generator_bot](https://t.me/YT_Thumbnail_generator_bot) bot untuk download Thumbnail Youtube



[gramjs]: https://github.com/gram-js/gramjs
